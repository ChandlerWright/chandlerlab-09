﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/TextureShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex("Diffuse Texture", 2D) = "white" {}
	}
	SubShader {
		Pass{
				CGPROGRAM
				#pragma vertex vertexFunction
				#pragma fragment fragmentFunction

				uniform float4 _Color;
				uniform sampler2D _MainTex;
				uniform float4 _MainTex_ST;

				struct inputStruct{
					float4 vertexPos : POSITION;
					float4 textureCoord : TEXCOORD0;
				};

				struct outputStruct{
					float4 pixelPos: SV_POSITION;
					float4 tex: TEXCOORD0;
				};

				outputStruct vertexFunction(inputStruct input){
					outputStruct toReturn;
					toReturn.pixelPos = UnityObjectToClipPos(input.vertexPos);

					toReturn.tex = input.textureCoord;

					return toReturn;
				}

				float4 fragmentFunction(outputStruct input) : COLOR{
					float4 tex = tex2D(_MainTex, input.tex.xy * input.tex.xy + _MainTex_ST.zw);
					return _Color * tex;
				}

				ENDCG
			}
	}
	FallBack "Diffuse"
}
